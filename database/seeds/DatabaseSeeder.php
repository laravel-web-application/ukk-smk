<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            "name" => "Uzumaki Naruto",
            "email" => "uzumaki_naruto@konohagakure.co.jp",
            "hak_akses" => "admin",
            "password" => Hash::make('admin')
        ]);
    }
}
